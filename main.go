package main

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	"github.com/urfave/cli"
)

const (
	Name        = "Robottle service client"
	Version     = "0.0.1"
	Email       = "coderoso.io@gmail.com"
	Author      = "Saul Martínez"
	Usage       = "A command line application to communicate with Robottle services."
	CommandName = "robottle"
)

func main() {
	var ()

	app := cli.NewApp()
	app.Name = Name
	app.Usage = Usage
	app.HelpName = CommandName
	app.Email = Email
	app.Version = Version
	app.Author = Author
	app.Flags = []cli.Flag{
		hostFlag,
		portFlag,
		secureFlag,
		tokenFlag,
		tokenFileFlag,
	}
	app.Before = func(c *cli.Context) (err error) {
		if c.GlobalIsSet("token-file") {
			if !filepath.IsAbs(tokenFile) {
				if tokenFile, err = filepath.Abs(tokenFile); err != nil {
					return
				}
			}
			if _, err = os.Stat(tokenFile); os.IsNotExist(err) {
				return
			}
			log.Printf("Using token file: %s", tokenFile)
			contents := make([]byte, 0)
			contents, err = ioutil.ReadFile(tokenFile)
			if err != nil {
				return
			}
			token = string(contents)
		}
		client := NewHTTPClient(apiHost, apiPort, apiSecure)

		app.Commands = []cli.Command{
			authCommand(client),
			accountCommand(client),
			userCommand(client),
			deviceCommand(client),
			simulateCommand(client),
		}
		return nil
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

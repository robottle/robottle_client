package main

import (
	"encoding/hex"
	"fmt"
	"os"

	"gitlab.com/robottle/robottle_common/hashing"

	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli"

	proto "gitlab.com/robottle/robottle_device/proto/device"
)

func deviceCommand(client *HTTPClient) cli.Command {
	return cli.Command{
		Name:  "device",
		Usage: "Device operations",
		Subcommands: []cli.Command{
			{
				Name:     "list",
				Usage:    "List account devices",
				Category: "device",
				Flags: []cli.Flag{
					deviceNameFlag,
					deviceDescriptionFlag,
				},
				Before: validateFlags,
				Action: listDevices(client),
			},
			{
				Name:     "create",
				Usage:    "Create a new device",
				Category: "device",
				Flags: []cli.Flag{
					deviceIDFlag,
					deviceNameFlag,
					deviceDescriptionFlag,
				},
				Before: validateFlags,
				Action: createDevice(client),
			},
			{
				Name:     "update",
				Usage:    "Update an existing device",
				Category: "device",
				Flags: []cli.Flag{
					deviceIDFlag,
					deviceNameFlag,
					deviceDescriptionFlag,
				},
				Before: validateFlags,
				Action: updateDevice(client),
			},
			{
				Name:     "regenerate-keys",
				Usage:    "Regenerate device keys",
				Category: "device",
				Flags: []cli.Flag{
					deviceIDFlag,
				},
				Before: validateFlags,
				Action: regenerateDeviceKeys(client),
			},
			{
				Name:     "send-data",
				Usage:    "Send data to collector",
				Category: "device",
				Flags: []cli.Flag{
					deviceKeyFlag,
					deviceSecretFlag,
					deviceDataFlag,
				},
				Before: validateFlags,
				Action: sendDeviceData(client),
			},
		},
	}
}

func sendDeviceData(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) (err error) {
		signature := signPayload([]byte(deviceData))
		err = client.SendDeviceData(deviceKey, signature, []byte(deviceData))
		return
	}
}

func signPayload(payload []byte) (signature string) {
	hmac := &hashing.HMAC{}
	sign := hmac.SignHMAC([]byte(payload), []byte(deviceSecret))
	signature = "sha512=" + hex.EncodeToString(sign)
	return
}

func createDevice(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		req := &proto.DeviceRequest{
			Name:        deviceName,
			Description: deviceDescription,
		}
		res, err := client.CreateDevice(req)
		if err != nil {
			return err
		}
		displayDevices(res)
		return nil
	}
}

func listDevices(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		res, err := client.ListDevices()
		if err != nil {
			return err
		}
		displayDevices(res.Devices...)
		return nil
	}
}

func updateDevice(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		if deviceID <= 0 {
			return fmt.Errorf("invalid device id")
		}
		req := &proto.DeviceRequest{
			Id:          deviceID,
			Name:        deviceName,
			Description: deviceDescription,
		}
		res, err := client.UpdateDevice(req)
		if err != nil {
			return err
		}
		displayDevices(res)
		return nil
	}
}

func regenerateDeviceKeys(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		if deviceID <= 0 {
			return fmt.Errorf("invalid device id")
		}
		req := &proto.DeviceRequest{
			Id: deviceID,
		}
		res, err := client.RegenerateDeviceKeys(req)
		if err != nil {
			return err
		}
		displayDevices(res)
		return nil
	}
}

func displayDevices(devices ...*proto.DeviceResponse) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{
		"ID",
		// "Account ID",
		"Name",
		"Slug",
		// "Description",
		"Key",
		"Secret",
		// "Created at",
		// "Updated at",
	})
	for _, device := range devices {
		row := []string{
			fmt.Sprintf("%d", device.Id),
			// fmt.Sprintf("%d", device.AccountId),
			device.Name,
			device.Slug,
			// device.Description,
			device.DeviceKey,
			device.DeviceSecret,
			// device.CreatedAt,
			// device.UpdatedAt,
		}
		table.Append(row)
	}
	table.Render()
}

package main

import (
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli"

	proto "gitlab.com/robottle/robottle_account/proto/account"
)

func userCommand(client *HTTPClient) cli.Command {
	return cli.Command{
		Name:  "user",
		Usage: "User operations",
		Flags: []cli.Flag{
			tokenFlag,
		},
		Subcommands: []cli.Command{
			{
				Name:  "create",
				Usage: "Creates a new user",
				Flags: []cli.Flag{
					userNameFlag,
					userEmailFlag,
					userPasswordFlag,
					userPasswordConfirmationFlag,
				},
				Before:   validateFlags,
				Category: "user",
				Action:   createUser(client),
			},
			{
				Name:  "update",
				Usage: "Updates an existing user",
				Flags: []cli.Flag{
					userIDFlag,
					userNameFlag,
					userPasswordFlag,
					userPasswordConfirmationFlag,
				},
				Before:   validateFlags,
				Category: "user",
				Action:   updateUser(client),
			},
			{
				Name:     "list",
				Usage:    "List the users for the current user's account",
				Before:   validateFlags,
				Category: "user",
				Action:   listUsers(client),
			},
		},
	}
}

func createUser(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		req := &proto.UserRequest{
			Id:                   userID,
			Name:                 userName,
			Email:                userEmail,
			Password:             userPassword,
			PasswordConfirmation: userPasswordConfirmation,
		}
		res, err := client.CreateUser(req)
		if err != nil {
			return err
		}
		displayUsers(res)
		return nil
	}
}

func updateUser(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		if userID <= 0 {
			return fmt.Errorf("invalid user id")
		}
		req := &proto.UserRequest{
			Id:                   userID,
			Name:                 userName,
			Password:             userPassword,
			PasswordConfirmation: userPasswordConfirmation,
		}
		res, err := client.UpdateUser(req)
		if err != nil {
			return err
		}
		displayUsers(res)
		return nil
	}
}

func listUsers(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		res, err := client.ListUsers()
		if err != nil {
			return err
		}
		displayUsers(res.Users...)
		return nil
	}
}

func displayUsers(users ...*proto.UserResponse) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader(
		[]string{"ID", "AccountID", "Name", "Email", "Created at", "Updated at"},
	)
	for _, user := range users {
		row := []string{
			fmt.Sprintf("%d", user.Id),
			fmt.Sprintf("%d", user.AccountId),
			user.Name,
			user.Email,
			user.CreatedAt,
			user.UpdatedAt,
		}
		table.Append(row)
	}
	table.Render()
}

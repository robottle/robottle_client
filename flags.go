package main

import (
	"fmt"

	"github.com/urfave/cli"
)

var (
	apiHost                   string
	apiPort                   int
	apiSecure                 bool
	token                     string
	tokenFile                 string
	accountName               string
	ownerName                 string
	ownerEmail                string
	ownerPassword             string
	ownerPasswordConfirmation string
	userID                    int64
	userName                  string
	userEmail                 string
	userPassword              string
	userPasswordConfirmation  string
	authEmail                 string
	authPassword              string
	deviceID                  int64
	deviceName                string
	deviceDescription         string
	deviceData                string
	deviceKey                 string
	deviceSecret              string
	deviceSensor              string
	city                      string
	weatherUnits              string

	hostFlag = cli.StringFlag{
		Name:        "api-host",
		Usage:       "API apiHost. Defaults to localhost",
		Value:       "localhost",
		Destination: &apiHost,
	}
	portFlag = cli.IntFlag{
		Name:        "api-port",
		Usage:       "API apiPort. Defaults to 8080",
		Value:       8080,
		Destination: &apiPort,
	}
	secureFlag = cli.BoolFlag{
		Name:        "api-secure",
		Usage:       "Use apiSecure API. Defaults to false",
		Destination: &apiSecure,
	}
	tokenFileFlag = cli.StringFlag{
		Name:        "token-file",
		Usage:       "Read authentication token from TOKE_FILE",
		Value:       "token.txt",
		Destination: &tokenFile,
	}
	tokenFlag = cli.StringFlag{
		Name:        "token",
		Usage:       "Authentication token",
		Destination: &token,
	}
	accountNameFlag = cli.StringFlag{
		Name:        "account-name",
		Usage:       "Account name",
		Destination: &accountName,
	}
	ownerNameFlag = cli.StringFlag{
		Name:        "owner-name",
		Usage:       "Owner name",
		Destination: &ownerName,
	}
	ownerEmailFlag = cli.StringFlag{
		Name:        "owner-email",
		Usage:       "Owner email",
		Destination: &ownerEmail,
	}
	ownerPasswordFlag = cli.StringFlag{
		Name:        "owner-password",
		Usage:       "Owner password",
		Destination: &ownerPassword,
	}
	ownerPasswordConfirmationFlag = cli.StringFlag{
		Name:        "owner-password-confirmation",
		Usage:       "Owner password confirmation",
		Destination: &ownerPasswordConfirmation,
	}
	userIDFlag = cli.Int64Flag{
		Name:        "id",
		Usage:       "User ID",
		Destination: &userID,
	}
	userNameFlag = cli.StringFlag{
		Name:        "name",
		Usage:       "User name",
		Destination: &userName,
	}
	userEmailFlag = cli.StringFlag{
		Name:        "email",
		Usage:       "User email",
		Destination: &userEmail,
	}
	userPasswordFlag = cli.StringFlag{
		Name:        "password",
		Usage:       "User password",
		Destination: &userPassword,
	}
	userPasswordConfirmationFlag = cli.StringFlag{
		Name:        "password-confirmation",
		Usage:       "User password confirmation",
		Destination: &userPasswordConfirmation,
	}
	authEmailFlag = cli.StringFlag{
		Name:        "email",
		Usage:       "Authentication email",
		Destination: &authEmail,
	}
	authPasswordFlag = cli.StringFlag{
		Name:        "password",
		Usage:       "Authentication password",
		Destination: &authPassword,
	}
	deviceIDFlag = cli.Int64Flag{
		Name:        "id",
		Usage:       "Device ID",
		Destination: &deviceID,
	}
	deviceNameFlag = cli.StringFlag{
		Name:        "name",
		Usage:       "Device name",
		Destination: &deviceName,
	}
	deviceDescriptionFlag = cli.StringFlag{
		Name:        "description",
		Usage:       "Device description",
		Destination: &deviceDescription,
	}
	deviceKeyFlag = cli.StringFlag{
		Name:        "key",
		Usage:       "Device key",
		Destination: &deviceKey,
	}
	deviceSecretFlag = cli.StringFlag{
		Name:        "secret",
		Usage:       "Device secret",
		Destination: &deviceSecret,
	}
	deviceDataFlag = cli.StringFlag{
		Name:        "data",
		Usage:       "Device data",
		Destination: &deviceData,
	}
	deviceSensorFlag = cli.StringFlag{
		Name:        "device-sensor",
		Usage:       "Device sensor",
		Destination: &deviceSensor,
	}
	cityFlag = cli.StringFlag{
		Name:        "city",
		Usage:       "City to fetch weather for",
		Value:       "Mexico City",
		Destination: &city,
	}
	weatherUnitsFlag = cli.StringFlag{
		Name:        "units",
		Usage:       "Weather units, metric (Celcius) or imperial (Fahrenheit). Default metric",
		Value:       "metric",
		Destination: &weatherUnits,
	}

	optionalFlags = []string{
		"id",
		"token",
		"name",
		"description",
		"password",
		"password-confirmation",
	}
)

func validateFlags(c *cli.Context) error {
	for _, flag := range c.FlagNames() {
		if !c.IsSet(flag) && !isOptional(flag) {
			return fmt.Errorf("--%s not set", flag)
		}
	}
	return nil
}
func isOptional(flag string) bool {
	for _, optional := range optionalFlags {
		if flag == optional {
			return true
		}
	}
	return false
}

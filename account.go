package main

import (
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli"

	proto "gitlab.com/robottle/robottle_account/proto/account"
)

func accountCommand(client *HTTPClient) cli.Command {
	return cli.Command{
		Name:  "account",
		Usage: "Account operations",
		Subcommands: []cli.Command{
			{
				Name:  "create",
				Usage: "Create a new account",
				Flags: []cli.Flag{
					accountNameFlag,
					ownerNameFlag,
					ownerEmailFlag,
					ownerPasswordFlag,
					ownerPasswordConfirmationFlag,
				},
				Category: "account",
				Before:   validateFlags,
				Action:   createAccount(client),
			},
			{
				Name:  "list",
				Usage: "List accounts",
				Flags: []cli.Flag{
					tokenFlag,
				},
				Category: "account",
				Action:   listAccounts(client),
			},
		},
	}
}

func listAccounts(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		res, err := client.ListAccounts()
		if err != nil {
			return err
		}
		displayAccounts(res.Accounts...)
		return nil
	}
}

func createAccount(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		req := &proto.AccountRequest{
			Name: accountName,
			Owner: &proto.UserRequest{
				Name:                 ownerName,
				Email:                ownerEmail,
				Password:             ownerPassword,
				PasswordConfirmation: ownerPasswordConfirmation,
			},
		}
		res, err := client.CreateAccount(req)
		if err != nil {
			return err
		}
		displayAccounts(res)
		return nil
	}
}

func displayAccounts(accounts ...*proto.AccountResponse) {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{
		"ID", "Name", "Owner", "Email", "Created at", "Updated at",
	})
	for _, account := range accounts {
		row := []string{
			fmt.Sprintf("%d", account.Id),
			account.Name,
			account.Owner.Name,
			account.Owner.Email,
			account.CreatedAt,
			account.UpdatedAt,
		}
		table.Append(row)
	}
	table.Render()
}

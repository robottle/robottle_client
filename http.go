package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/gojektech/heimdall/httpclient"

	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	deviceProto "gitlab.com/robottle/robottle_device/proto/device"
)

type customHTTPClient struct {
	client http.Client
}

type HTTPClient struct {
	*httpclient.Client

	baseURL string
}

func NewHTTPClient(host string, port int, secure bool) *HTTPClient {
	scheme := "http"
	if secure {
		scheme = "https"
	}
	baseURL := fmt.Sprintf("%s://%s:%d", scheme, host, port)
	return &HTTPClient{
		Client: httpclient.NewClient(
			httpclient.WithHTTPClient(&customHTTPClient{
				client: http.Client{},
			})),
		baseURL: baseURL,
	}
}

func (c *customHTTPClient) Do(request *http.Request) (*http.Response, error) {
	if request.Header == nil {
		request.Header = http.Header{}
	}
	if len(token) > 0 {
		authorization := fmt.Sprintf("JWT %s", token)
		request.Header.Set("Authorization", authorization)
	}
	return c.client.Do(request)
}

func (c *HTTPClient) ListAccounts() (*accountProto.ListAccountsResponse, error) {
	url := fmt.Sprintf("%s/account/accounts/", c.baseURL)
	res, err := c.Get(url, nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, responseError(body, res.Status)
	}
	list := &accountProto.ListAccountsResponse{}
	if err := json.Unmarshal(body, &list); err != nil {
		return nil, err
	}
	return list, err
}

func (c *HTTPClient) CreateAccount(req *accountProto.AccountRequest) (*accountProto.AccountResponse, error) {
	url := fmt.Sprintf("%s/account/accounts/", c.baseURL)
	bytes, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	res, err := c.Post(url, strings.NewReader(string(bytes)), nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusCreated {
		return nil, responseError(body, res.Status)
	}
	account := &accountProto.AccountResponse{}
	if err := json.Unmarshal(body, &account); err != nil {
		return nil, err
	}
	return account, nil
}

func (c *HTTPClient) CreateToken(req *accountProto.CreateTokenRequest) (*accountProto.TokenResponse, error) {
	url := fmt.Sprintf("%s/account/auth/", c.baseURL)
	bytes, err := json.Marshal(&req)
	if err != nil {
		return nil, err
	}
	res, err := c.Post(url, strings.NewReader(string(bytes)), nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusCreated {
		return nil, responseError(body, res.Status)
	}
	token := &accountProto.TokenResponse{}
	if err := json.Unmarshal(body, &token); err != nil {
		return nil, err
	}
	return token, nil
}

func (c *HTTPClient) VerifyToken(req *accountProto.VerifyTokenRequest) (*accountProto.TokenResponse, error) {
	url := fmt.Sprintf("%s/account/auth/", c.baseURL)
	res, err := c.Get(url, nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, responseError(body, res.Status)
	}
	token := &accountProto.TokenResponse{}
	if err := json.Unmarshal(body, &token); err != nil {
		return nil, err
	}
	return token, nil
}

func (c *HTTPClient) RevokeToken(req *accountProto.RevokeTokenRequest) error {
	url := fmt.Sprintf("%s/account/auth", c.baseURL)
	res, err := c.Delete(url, nil)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusNoContent {
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return err
		}
		return responseError(body, res.Status)
	}
	return nil
}

func (c *HTTPClient) CreateUser(req *accountProto.UserRequest) (*accountProto.UserResponse, error) {
	url := fmt.Sprintf("%s/account/users/", c.baseURL)
	bytes, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	res, err := c.Post(url, strings.NewReader(string(bytes)), nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusCreated {
		return nil, responseError(body, res.Status)
	}
	user := &accountProto.UserResponse{}
	if err := json.Unmarshal(body, &user); err != nil {
		return nil, err
	}
	return user, nil
}

func (c *HTTPClient) UpdateUser(req *accountProto.UserRequest) (*accountProto.UserResponse, error) {
	url := fmt.Sprintf("%s/account/users/%d", c.baseURL, req.Id)
	bytes, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	res, err := c.Put(url, strings.NewReader(string(bytes)), nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, responseError(body, res.Status)
	}
	user := &accountProto.UserResponse{}
	if err := json.Unmarshal(body, &user); err != nil {
		return nil, err
	}
	return user, nil
}

func (c *HTTPClient) ListUsers() (*accountProto.ListUsersResponse, error) {
	url := fmt.Sprintf("%s/account/users/", c.baseURL)
	res, err := c.Get(url, nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, responseError(body, res.Status)
	}
	users := &accountProto.ListUsersResponse{}
	if err := json.Unmarshal(body, &users); err != nil {
		return nil, err
	}
	return users, nil
}

func (c *HTTPClient) CreateDevice(req *deviceProto.DeviceRequest) (*deviceProto.DeviceResponse, error) {
	url := fmt.Sprintf("%s/device/devices/", c.baseURL)
	bytes, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	res, err := c.Post(url, strings.NewReader(string(bytes)), nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusCreated {
		return nil, responseError(body, res.Status)
	}
	device := &deviceProto.DeviceResponse{}
	if err := json.Unmarshal(body, &device); err != nil {
		return nil, err
	}
	return device, nil
}

func (c *HTTPClient) UpdateDevice(req *deviceProto.DeviceRequest) (*deviceProto.DeviceResponse, error) {
	url := fmt.Sprintf("%s/device/devices/%d", c.baseURL, req.Id)
	bytes, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	res, err := c.Put(url, strings.NewReader(string(bytes)), nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, responseError(body, res.Status)
	}
	device := &deviceProto.DeviceResponse{}
	if err := json.Unmarshal(body, &device); err != nil {
		return nil, err
	}
	return device, nil
}

func (c *HTTPClient) RegenerateDeviceKeys(req *deviceProto.DeviceRequest) (*deviceProto.DeviceResponse, error) {
	url := fmt.Sprintf("%s/device/devices/%d/regenerate_keys", c.baseURL, req.Id)
	res, err := c.Put(url, nil, nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, responseError(body, res.Status)
	}
	device := &deviceProto.DeviceResponse{}
	if err := json.Unmarshal(body, &device); err != nil {
		return nil, err
	}
	return device, nil
}

func (c *HTTPClient) ListDevices() (*deviceProto.ListDevicesResponse, error) {
	url := fmt.Sprintf("%s/device/devices/", c.baseURL)
	res, err := c.Get(url, nil)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	if res.StatusCode != http.StatusOK {
		return nil, responseError(body, res.Status)
	}
	devices := &deviceProto.ListDevicesResponse{}
	if err := json.Unmarshal(body, &devices); err != nil {
		return nil, err
	}
	return devices, nil
}

func (c *HTTPClient) SendDeviceData(deviceKey string, signature string, deviceData []byte) error {
	url := fmt.Sprintf("%s/collector/data/", c.baseURL)
	if len(deviceSensor) > 0 {
		url = fmt.Sprintf("%s?sensor=%s", url, deviceSensor)
	}
	headers := http.Header{
		"X-Device-Key": []string{deviceKey},
		"X-Signature":  []string{signature},
	}
	res, err := c.Post(url, bytes.NewReader(deviceData), headers)
	if err != nil {
		return err
	}
	if res.StatusCode != http.StatusNoContent {
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return err
		}
		return responseError(body, res.Status)
	}
	return nil
}

func responseError(body []byte, status string) (err error) {
	err = fmt.Errorf(status)
	if body != nil {
		err = fmt.Errorf("%+v", string(body))
	}
	return
}

package main

import (
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
	"github.com/urfave/cli"

	proto "gitlab.com/robottle/robottle_account/proto/account"
)

func authCommand(client *HTTPClient) cli.Command {
	return cli.Command{
		Name:  "auth",
		Usage: "Authentication operations",
		Flags: []cli.Flag{
			tokenFlag,
		},
		Subcommands: []cli.Command{
			{
				Name:   "create-token",
				Usage:  "Create a new authentication token",
				Before: validateFlags,
				Flags: []cli.Flag{
					authEmailFlag,
					authPasswordFlag,
				},
				Category: "auth",
				Action:   createToken(client),
			},
			{
				Name:   "verify-token",
				Usage:  "Verify the validity of a token",
				Before: validateFlags,
				Flags: []cli.Flag{
					tokenFlag,
				},
				Action: verifyToken(client),
			},
			{
				Name:   "revoke-token",
				Usage:  "Revoke an authentiaction token",
				Before: validateFlags,
				Flags: []cli.Flag{
					tokenFlag,
				},
				Action: revokeToken(client),
			},
		},
	}
}

func revokeToken(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) (err error) {
		if err != nil {
			return
		}
		req := &proto.RevokeTokenRequest{
			Token: token,
		}
		err = client.RevokeToken(req)
		return
	}
}

func verifyToken(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) (err error) {
		if err != nil {
			return
		}
		req := &proto.VerifyTokenRequest{
			Token: token,
		}
		res, err := client.VerifyToken(req)
		if err != nil {
			return err
		}
		displayToken(res)
		return
	}
}

func createToken(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		req := &proto.CreateTokenRequest{
			Email:    authEmail,
			Password: authPassword,
		}
		res, err := client.CreateToken(req)
		if err != nil {
			return err
		}
		displayToken(res)
		return nil
	}
}

func displayToken(token *proto.TokenResponse) {
	data := [][]string{
		{
			fmt.Sprintf("%d", token.User.Id),
			fmt.Sprintf("%d", token.User.AccountId),
			token.User.Name,
			token.User.Email,
			token.User.CreatedAt,
			token.User.UpdatedAt,
		},
	}
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{
		"User ID", "Account ID", "Name", "Email", "Created at", "Updated at",
	})
	table.SetAutoWrapText(true)
	table.SetAutoMergeCells(true)
	table.SetRowLine(true)
	table.AppendBulk(data)
	table.Render()
	fmt.Printf("Token: %s\n", token.Token)
}

module gitlab.com/robottle/robottle_client

go 1.12

require (
	github.com/gojektech/heimdall v5.0.2+incompatible // indirect
	github.com/gojektech/valkyrie v0.0.0-20190210220504-8f62c1e7ba45 // indirect
	github.com/micro/go-micro v1.1.0
	github.com/olekukonko/tablewriter v0.0.1 // indirect
	github.com/urfave/cli v1.20.0 // indirect
	gitlab.com/robottle/robottle_account v0.0.0-20190503011625-7e0872a308a3
	gitlab.com/robottle/robottle_common v0.0.0-20190502075711-c703ea102562
	gitlab.com/robottle/robottle_device v0.0.0-00010101000000-000000000000 // indirect
)

replace gitlab.com/robottle/robottle_common => ../robottle_common

replace gitlab.com/robottle/robottle_account => ../robottle_account

replace gitlab.com/robottle/robottle_device => ../robottle_device

LDFLAGS := -ldflags="-s -w"
BIN := ./robottle

.PHONY: all dep build clean test lint proto

all: build

build: dep
	go build -i ${LDFLAGS} -o ${BIN} *.go

dep:
	go mod download

vendor:
	go mod vendor

run: build
	${BIN}

clean:
	rm -rf ${BIN}

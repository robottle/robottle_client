package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/gojektech/heimdall/httpclient"

	"github.com/olekukonko/tablewriter"
	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/mem"

	"github.com/urfave/cli"
)

var (
	openWeatherURL    = "https://api.openweathermap.org/data/2.5/weather"
	openWeatherAPIKey = os.Getenv("OPEN_WEATHER_API_KEY")
)

type memoryUsage struct {
	Total       uint64  `json:"total"`
	Available   uint64  `json:"available"`
	Active      uint64  `json:"active"`
	Free        uint64  `json:"free"`
	Used        uint64  `json:"used"`
	UsedPercent float64 `json:"used_percent"`
	SwapTotal   uint64  `json:"swap_total"`
	SwapCached  uint64  `json:"swap_cached"`
	SwapFree    uint64  `json:"swap_free"`
}

func (m *memoryUsage) String() string {
	return fmt.Sprintf(
		"Total: %v, Available: %v, Free %s, Used: %d (%f.2%%)",
		m.Total, m.Available, m.Free, m.Used, m.UsedPercent,
	)
}

func simulateCommand(client *HTTPClient) cli.Command {
	return cli.Command{
		Name:   "simulate",
		Usage:  "Simulate device data",
		Before: validateFlags,
		Subcommands: []cli.Command{
			{
				Name:  "cpu",
				Usage: "Simulate CPU usage",
				Flags: []cli.Flag{
					deviceKeyFlag,
					deviceSecretFlag,
					deviceSensorFlag,
				},
				Action: simulateCPUUsage(client),
			},
			{
				Name:  "memory",
				Usage: "Simulate memory usage",
				Flags: []cli.Flag{
					deviceKeyFlag,
					deviceSecretFlag,
					deviceSensorFlag,
				},
				Action: simulateMemory(client),
			},
			{
				Name:  "load",
				Usage: "Simulate system load",
				Flags: []cli.Flag{
					deviceKeyFlag,
					deviceSecretFlag,
					deviceSensorFlag,
				},
				Action: simulateLoad(client),
			},
			{
				Name:  "weather",
				Usage: "Simulate weather",
				Flags: []cli.Flag{
					deviceKeyFlag,
					deviceSecretFlag,
					cityFlag,
					weatherUnitsFlag,
					deviceSensorFlag,
				},
				Action: simulateWeather(client),
			},
		},
	}
}

func simulateWeather(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		errc := make(chan error)

		directions := map[int64]string{
			1:  "N",
			2:  "NNE",
			3:  "NE",
			4:  "ENE",
			5:  "E",
			6:  "ESE",
			7:  "SE",
			8:  "SSE",
			9:  "S",
			10: "SSW",
			11: "SW",
			12: "WSW",
			13: "W",
			14: "WNW",
			15: "NW",
			16: "NNW",
			17: "N",
		}
		degreesToDirection := func(degrees float64) string {
			reminder := int64(math.Round(math.Mod(degrees, 360.0)/22.5)) + 1
			log.Printf("degrees: %f, reminder: %d", degrees, reminder)
			direction, found := directions[reminder]
			if !found {
				direction = "Unknown"
			}
			return direction
		}

		type weatherResponse struct {
			City string `json:"name"`
			Main struct {
				Temperature float64 `json:"temp"`
				Pressure    int64   `json:"pressure"`
				Humidity    int64   `json:"humidity"`
				Min         float64 `json:"temp_min"`
				Max         float64 `json:"temp_max"`
			} `json:"main"`
			Wind struct {
				Speed   float64 `json:"speed"`
				Degrees float64 `json:"deg"`
			}
		}

		type weatherInfo struct {
			City        string  `json:"city"`
			Temperature float64 `json:"temperature"`
			Pressure    int64   `json:"pressure"`
			Humidity    int64   `json:"humidity"`
			Min         float64 `json:"min"`
			Max         float64 `json:"max"`
			WindSpeed   float64 `json:"wind_speed"`
			WindDegrees float64 `json:"wind_degrees"`
		}

		go func() {
			ticker := time.NewTicker(2 * time.Minute)
			for ; true; <-ticker.C {
				log.Printf("Getting weather data for %s", city)
				httpClient := httpclient.NewClient()
				url := fmt.Sprintf(
					"%s?q=%s&units=%s&appid=%s",
					openWeatherURL,
					city,
					weatherUnits,
					openWeatherAPIKey,
				)
				res, err := httpClient.Get(url, nil)
				if err != nil {
					log.Printf("error getting weather data: %v", err)
					continue
				}
				body, err := ioutil.ReadAll(res.Body)
				if err != nil {
					log.Printf("error reading body response: %v", err)
					continue
				}
				weatherResponse := &weatherResponse{}
				if err := json.Unmarshal(body, &weatherResponse); err != nil {
					log.Printf("error unmarshaling weather response: %v", err)
					return
				}
				log.Printf(
					"City: %s %f.1°C\nMin: %f.1°C\nMax: %f.1°C\nWind: %f.2 (%s)\nHumidity: %d%%",
					weatherResponse.City,
					weatherResponse.Main.Temperature,
					weatherResponse.Main.Min,
					weatherResponse.Main.Max,
					weatherResponse.Wind.Speed,
					degreesToDirection(weatherResponse.Wind.Degrees),
					weatherResponse.Main.Humidity,
				)
				weatherInfo := &weatherInfo{
					City:        weatherResponse.City,
					Temperature: weatherResponse.Main.Temperature,
					Pressure:    weatherResponse.Main.Pressure,
					Humidity:    weatherResponse.Main.Humidity,
					Min:         weatherResponse.Main.Min,
					Max:         weatherResponse.Main.Max,
					WindSpeed:   weatherResponse.Wind.Speed,
					WindDegrees: weatherResponse.Wind.Degrees,
				}
				bytes, err := json.Marshal(weatherInfo)
				if err != nil {
					log.Printf("error marshaling sample: %v", err)
					continue
				}
				signature := signPayload(bytes)
				if err := client.SendDeviceData(deviceKey, signature, bytes); err != nil {
					log.Printf("error sending device data: %v", err)
					continue
				}
			}
		}()

		go func() {
			c := make(chan os.Signal)
			signal.Notify(c, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL)
			errc <- fmt.Errorf("%v", <-c)
		}()
		return <-errc
	}
}

func simulateLoad(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		errc := make(chan error)

		go func() {
			ticker := time.NewTicker(1 * time.Second)
			for range ticker.C {
				l, err := load.Avg()
				if err != nil {
					log.Printf("error getting load average: %v", err)
					continue
				}
				log.Printf("load: %s", l.String())
				m, err := load.Misc()
				if err != nil {
					log.Printf("error getting processes: %v", err)
					continue
				}
				log.Printf("processes: %s", m.String())

				sample := map[string]interface{}{
					"load":    l.Load1,
					"running": m.ProcsRunning,
					"blocked": m.ProcsBlocked,
				}

				bytes, err := json.Marshal(sample)
				if err != nil {
					log.Printf("error marshaling sample: %v", err)
					continue
				}
				signature := signPayload(bytes)
				if err := client.SendDeviceData(deviceKey, signature, bytes); err != nil {
					log.Printf("error sending device data: %v", err)
					continue
				}
			}
		}()

		go func() {
			c := make(chan os.Signal)
			signal.Notify(c, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL)
			errc <- fmt.Errorf("%v", <-c)
		}()
		return <-errc
	}
}

func simulateMemory(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		errc := make(chan error)

		go func() {
			ticker := time.NewTicker(1 * time.Second)
			for range ticker.C {
				m, err := mem.VirtualMemory()
				if err != nil {
					continue
				}

				memory := &memoryUsage{
					Total:       m.Total,
					Available:   m.Available,
					Active:      m.Active,
					Free:        m.Free,
					Used:        m.Used,
					UsedPercent: m.UsedPercent,
					SwapTotal:   m.SwapTotal,
					SwapCached:  m.SwapCached,
					SwapFree:    m.SwapFree,
				}
				bytes, err := json.Marshal(memory)
				if err != nil {
					log.Printf("error marshaling CPU usage: %v", err)
					continue
				}
				signature := signPayload(bytes)
				if err := client.SendDeviceData(deviceKey, signature, bytes); err != nil {
					log.Printf("error sending device data: %v", err)
					continue
				}
				log.Printf("%s", memory)
			}
		}()

		go func() {
			c := make(chan os.Signal)
			signal.Notify(c, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL)
			errc <- fmt.Errorf("%v", <-c)
		}()
		return <-errc
	}
}

func simulateCPUUsage(client *HTTPClient) cli.ActionFunc {
	return func(c *cli.Context) error {
		return nil
	}
}

func displayMemoryUsage(memory *memoryUsage) {
	tableBuilder := &strings.Builder{}
	table := tablewriter.NewWriter(tableBuilder)
	table.SetHeader(
		[]string{
			"Total",
			"Available",
			"Active",
			"Free",
			"Used",
			"UsedPercent",
			"SwapTotal",
			"SwapCached",
			"SwapFree",
		},
	)
	table.Append([]string{
		fmt.Sprintf("%d", memory.Total),
		fmt.Sprintf("%d", memory.Available),
		fmt.Sprintf("%d", memory.Active),
		fmt.Sprintf("%d", memory.Free),
		fmt.Sprintf("%d", memory.Used),
		fmt.Sprintf("%f", memory.UsedPercent),
		fmt.Sprintf("%d", memory.SwapTotal),
		fmt.Sprintf("%d", memory.SwapCached),
		fmt.Sprintf("%d", memory.SwapFree),
	})
	table.Render()
	fmt.Printf("\033[2K\r%s", tableBuilder)
}

func displayCPUUsage(idle uint64, total uint64, timestamp time.Time) {
	tableString := &strings.Builder{}
	table := tablewriter.NewWriter(tableString)
	table.SetHeader(
		[]string{"Time", "Idle", "Total"},
	)
	table.Append([]string{
		timestamp.Format(time.Stamp),
		fmt.Sprintf("%d", idle),
		fmt.Sprintf("%d", total),
	})
	table.Render()

	fmt.Printf("\r%s", tableString)
}
